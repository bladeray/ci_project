# ci_project



## В pipeline реализованы:

- lint-test-job: проверка линтерами директории
- ui-test-job: запуск ui-тестов и сохранение allure-results в артефакты
- api-test-job: запуск api-тестов
- allure_job: генерация отчета allure и сохрание allure-reports в артефакты
- pages: публикация allure-отчета в gitlab (Settings --> Pages)


## Способы запуска pipeline

- по созданию коммита
- по нажатию кнопки "Run pipeline" (CI/CD --> "Run pipeline" button)
- по расписанию (CI/CD --> Schedules)
- по вызову curl
```
curl -X POST \
     -F token=84675e5bf9a5b2a23a2db450e77272 \
     -F ref=main \
     https://gitlab.com/api/v4/projects/32209885/trigger/pipeline
```

## Переменные окружения

В api-тесте используется Facebook-token:
```python
TOKEN = os.environ.get("FB_TOKEN")
```
Он занесен в переменные CI/CD проекта (Settings --> CI/CD --> Variables)