import allure
import pytest
import selenium
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption('--executor', action='store', default='local', help='Choose execute: local, selenoid or ci')


@pytest.fixture(scope='function')
def browser(request):
    with allure.step('Открываем браузер'):
        if request.config.getoption('--executor') == 'local':
            browser = webdriver.Chrome()
        else:
            command_executor = 'http://localhost:4444/wd/hub'
            if request.config.getoption('--executor') == 'ci':
                command_executor = "http://selenoid__chrome:4444"
            options = webdriver.ChromeOptions()
            options.add_argument('ignore-certificate-errors')
            capabilities = {
                "browserName": "chrome",
                "version": "96.0",
                "env": ["TZ=Europe/Samara"]
            }
            browser = webdriver.Remote(
                command_executor=command_executor,
                desired_capabilities=capabilities,
                options=options
            )

    yield browser

    if request.config.getoption('--alluredir') == 'allure-results':
        env_file = open('allure-results/environment.properties', 'w+')
        env_file.write(f'Pytest version={pytest.__version__}'
                       f'\nSelenium version={selenium.__version__}')
        env_file.close()

    with allure.step('Закрываем браузер'):
        browser.quit()
