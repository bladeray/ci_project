import allure
import pytest


@pytest.mark.ui
def test_user_can_open_google(browser):
    with allure.step('Открываем страницу https://www.google.com/?safe=active&ssui=on'):
        browser.get('https://www.google.com/?safe=active&ssui=on')
    with allure.step('Проверяем, что адрес текущей страницы https://www.google.com/?safe=active&ssui=on'):
        assert browser.current_url == 'https://www.google.com/?safe=active&ssui=on'


@pytest.mark.ui
def test_user_can_open_google_failed(browser):
    with allure.step('Открываем страницу https://www.google.com/?safe=active&ssui=on'):
        browser.get('https://www.google.com/?safe=active&ssui=on')
    with allure.step('Проверяем, что адрес текущей страницы не https://www.google.com/?safe=active&ssui=on'):
        assert browser.current_url != 'https://www.google.com/?safe=active&ssui=on'


@pytest.mark.ui
@pytest.mark.skip
def test_user_can_open_google_skipped(browser):
    with allure.step('Открываем страницу https://www.google.com/?safe=active&ssui=on'):
        browser.get('https://www.google.com/?safe=active&ssui=on')
    with allure.step('Проверяем, что адрес текущей страницы https://www.google.com/?safe=active&ssui=on'):
        assert browser.current_url == 'https://www.google.com/?safe=active&ssui=on'
