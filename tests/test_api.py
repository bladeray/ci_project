import os

import pytest
import requests


HEADERS = {
    'accept': 'application/json'
}

TOKEN = os.environ.get("FB_TOKEN")


@pytest.mark.api
def test_get_superhero_by_name():
    response = requests.get(url=f'https://superheroapi.com/api/{TOKEN}/search/batman',
                            headers=HEADERS)
    response.raise_for_status()
